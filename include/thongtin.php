<!-- page -->
<div class="services-breadcrumb">
    <div class="agile_inner_breadcrumb">
        <div class="container">
            <ul class="w3_short">
                <li>
                    <a href="index.html">Trang chủ</a>
                    <i>|</i>
                </li>
                <li>Thông tin về chúng tôi</li>
            </ul>
        </div>
    </div>
</div>
<!-- //page -->

<!-- about -->
<div class="welcome py-sm-5 py-4">
    <div class="container py-xl-4 py-lg-2">
        <!-- tittle heading -->
        <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
            Thông tin về chúng tôi
        </h3>
        <!-- //tittle heading -->
        <div class="row">
            <div class="col-lg-6 welcome-left">
                <h3>Thông tin thành viên</h3>
                <h4 class="my-sm-3 my-2">- Bùi Hoàng Trí Nghĩa - 1711061989</h4>
                <h4 class="my-sm-3 my-2">- Phạm Đình Tân - 1711061</h4>
                <h4 class="my-sm-3 my-2">- Lê Công Nữ Trinh - 1711061</h4>
                <h3>Giảng viên</h3>
                <h4 class="my-sm-3 my-2">- Nguyễn Đình Ánh</h4>
            </div>
            <div class="col-lg-6 welcome-right-top mt-lg-0 mt-sm-5 mt-4">
                <img src="images/1608638115_694_Genshin-Impact-Ro-ri-thong-tin-ve-nhan-vat-sap.jpg" class="img-fluid" alt=" ">
            </div>
        </div>
    </div>
</div>
<!-- //about -->

<!-- testimonials -->
<!-- <div class="testimonials py-sm-5 py-4">
    <div class="container py-xl-4 py-lg-2"> -->
        <!-- tittle heading -->
        <!-- <h3 class="tittle-w3l text-center text-white mb-lg-5 mb-sm-4 mb-3">
            <span>O</span>ur
            <span>C</span>ustomers
            <span>S</span>ays
        </h3> -->
        <!-- tittle heading -->
        <!-- <div class="row gallery-index">
            <div class="col-sm-6 med-testi-grid">
                <div class="med-testi test-tooltip rounded p-4">
                    <p>"sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
                <div class="row med-testi-left my-5">
                    <div class="col-lg-2 col-3 w3ls-med-testi-img">
                        <img src="images/user.jpg" alt=" " class="img-fluid rounded-circle" />
                    </div>
                    <div class="col-lg-10 col-9 med-testi-txt">
                        <h4 class="font-weight-bold mb-lg-1 mb-2">Tyson</h4>
                        <p>fames ac turpis</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 med-testi-grid">
                <div class="med-testi test-tooltip rounded p-4">
                    <p>"sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
                <div class="row med-testi-left my-5">
                    <div class="col-lg-2 col-3 w3ls-med-testi-img">
                        <img src="images/user.jpg" alt=" " class="img-fluid rounded-circle" />
                    </div>
                    <div class="col-lg-10 col-9 med-testi-txt">
                        <h4 class="font-weight-bold mb-lg-1 mb-2">Alejandra</h4>
                        <p>fames ac turpis</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 med-testi-grid">
                <div class="med-testi test-tooltip rounded p-4">
                    <p>"sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
                <div class="row med-testi-left mt-sm-5 my-5">
                    <div class="col-lg-2 col-3 w3ls-med-testi-img">
                        <img src="images/user.jpg" alt=" " class="img-fluid rounded-circle" />
                    </div>
                    <div class="col-lg-10 col-9 med-testi-txt">
                        <h4 class="font-weight-bold mb-lg-1 mb-2">Charles</h4>
                        <p>fames ac turpis</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 med-testi-grid">
                <div class="med-testi test-tooltip rounded p-4">
                    <p>"sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
                <div class="row med-testi-left mt-5">
                    <div class="col-lg-2 col-3 w3ls-med-testi-img">
                        <img src="images/user.jpg" alt=" " class="img-fluid rounded-circle" />
                    </div>
                    <div class="col-lg-10 col-9 med-testi-txt">
                        <h4 class="font-weight-bold mb-lg-1 mb-2">Jessie</h4>
                        <p>fames ac turpis</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- //testimonials -->